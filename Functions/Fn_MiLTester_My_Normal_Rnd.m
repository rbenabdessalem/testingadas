% % %
%  Copyright (c) {2015-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
%  Raja Ben Abdessalem (benabdessalem@svv.lu)
%  Shiva Nejati (nejati@svv.lu)
%  Briand Lionel (briand@svv.lu)
%  Thomas Stifter (Thomas.Stifter@iee.lu)
%  All rights reserved.
%
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions are
%  met:
%
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
%
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%  POSSIBILITY OF SUCH DAMAGE.


function [out]=Fn_MiLTester_My_Normal_Rnd(Miu,Sigma)    
    persistent hval
    persistent even
    if(even)
        out=hval;
        even=0;
    else
        w=0;
        while(w<=0 || w>=1)
            x=2*rand(1)-1;
            y=2*rand(1)-1;
            w=x^2+y^2;
        end
        gval=Miu+x*Sigma*sqrt(-2*log(w)/w);
        hval=Miu+y*Sigma*sqrt(-2*log(w)/w);
        even=1;
        out=gval;
        if out>1
            out=1;
        end
        if out <-1
            out=-1;
        end
     
    end
    
end