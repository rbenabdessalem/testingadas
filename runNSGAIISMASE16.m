% % %
%  Copyright (c) {2015-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
%  Raja Ben Abdessalem (benabdessalem@svv.lu)
%  Shiva Nejati (nejati@svv.lu)
%  Briand Lionel (briand@svv.lu)
%  Thomas Stifter (Thomas.Stifter@iee.lu)
%  All rights reserved.
%
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions are
%  met:
%
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
%
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%  POSSIBILITY OF SUCH DAMAGE.


try
    %%%%%%%%%%%%%%%%%%%%%%
    %%% INITIALIZATION %%%
    %%%%%%%%%%%%%%%%%%%%%%    
    mfilepath=fileparts(which('runNSGAIISMASE16.m'));
    addpath(fullfile(mfilepath,'/Functions'));
    addpath(fullfile(mfilepath,'/GA'));
    addpath(fullfile(mfilepath,'/SM'));
    load_system(fullfile(mfilepath,'/Experiment_2_cs.mdl'));
    
    tic
    
    ST=10;
    Fn_MiLTester_SetSimulationTime(ST);
    MaxAlgorithmIterations=100;
    SimulationTimeStep=GetSimulationTimeStep();
    startTime = rem(now,1);
    modelRunningTime=0;
    chromosome=[];
    clear intermediate_chromosomeP
    clear intermediate_chromosomeB
    clear intermediate_chromosomeW
    clear InP
    clear InB
    clear InW
    clear saveFile
    clear childP
    clear childW
    clear childB
    
    
    pop=10; %population size
    
    % initalize search parameters
    
    M=3;%nbr_obj_funcs
    V=5;%nbr_inputs 
    K = M + V;
    
    min=[0; 20;35;40;1;1];
    max=[0; 85; 48;160;5;25];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% 1. NSGAII: Randomly create an initial population. %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
    
    NFIT=0;
    clear chromosome
    for i = 1 : pop
        numpop=i;
        display(numpop);
        AA=[];
        b=0;
        TotSim=10;
        chromosome(i,1)=30+i;
        for j = 2 : V+1
            chromosome(i,j) = (min(j) + (max(j) - min(j))*rand(1));
        end
        
        
        x0P=chromosome(i,2)
        y0P=chromosome(i,3)
        ThP=chromosome(i,4)
        v0P=chromosome(i,5)
        v0C=chromosome(i,6)
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 2. Prescan: Run simulations for the initial population. %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
        BestDist=60;
        MinDistPedestrianCar=100;
        TTCMIN=4;
        MinDistPedestrianAwa=50;
        
        %%%% Change position and orientation of Pedestrian
        %%%%%%%%% Generate the experiment %%%%%%%%%
        disp('Variables reinitialized...');
        disp('------------------------');
        startTime = rem(now,1);
        startTimeA = rem(now,1);
        Experiment_Name='Experiment_2'; % Hereby enter your experiment name
        PreScan_ExperimentName= [Experiment_Name '.pex']; % add the extension for the PreScan file
        Simulink_ModelName= [ Experiment_Name '_cs'];% add the extension for the Simulink file
        PreScan_ExperimentLocation = [pwd '\' PreScan_ExperimentName];
        simulationCount=0;
        k=0;
        A1 = cell(3,1);
        B1 = cell(3,1);
        NbrOfTagAndVal=0;
        
        
        NbrOfTagAndVal=NbrOfTagAndVal+1;
        A1{NbrOfTagAndVal}='x_Person';
        radiusvalue={num2str(x0P)};
        idx = randi(numel(radiusvalue),1) ;
        currentName = radiusvalue{idx};
        B1{NbrOfTagAndVal}=currentName;
        
        
        NbrOfTagAndVal=NbrOfTagAndVal+1;
        A1{NbrOfTagAndVal}='y_Person';
        radiusvalue={num2str(y0P)};
        idx = randi(numel(radiusvalue),1) ;
        currentName = radiusvalue{idx};
        B1{NbrOfTagAndVal}=currentName;
        
        NbrOfTagAndVal=NbrOfTagAndVal+1;
        A1{NbrOfTagAndVal}='Th_Person';
        radiusvalue={num2str(ThP)};
        idx = randi(numel(radiusvalue),1) ;
        currentName = radiusvalue{idx};
        B1{NbrOfTagAndVal}=currentName;
        
        
        for m=1:NbrOfTagAndVal
            clear Tag Val
            Tag(m,:) =A1{m};
            Val(m,:)=B1{m};
            dos( ['PreScan.CLI.exe -convertAndLoad ' PreScan_ExperimentLocation ' -set ' Tag(m,:) '=' Val(m,:)]);     % Change the value in PreScan
            dos( 'PreScan.CLI.exe -save');         % Save the PreScan experiment
            
        end
        
        dos ('PreScan.CLI.exe --realignPaths');
        dos( 'PreScan.CLI.exe -build');
        
        open_system(Simulink_ModelName);                 % Open the Simulink Model
        
        regenButtonHandle = find_system(Simulink_ModelName, 'FindAll', 'on', 'type', 'annotation','text','Regenerate');
        regenButtonCallbackText = get_param(regenButtonHandle,'ClickFcn');
        
        eval(regenButtonCallbackText);
        
        
        %Run Simulation
        %***
        sim(fullfile(mfilepath,'/Experiment_2_cs.mdl'));
        %***
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 3. NSGAII: Evaluate the objective functions for the initial population. %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%         
        AA=SimStopTime.time;
        b=numel(AA);
        TotSim=AA(b);
        [MinDistPedestrianCar,TTCMIN,MinDistPedestrianAwa]=ObjectiveFunctionsDistancesNew(TotSim,SimulationTimeStep, xCar,yCar,vCar,xPerson,yPerson,vPerson,ThP,TTCcol)
        
        NFIT=NFIT+1;
        
              
        
        chromosome(i,V+2)= MinDistPedestrianCar;
        chromosome(i,V+3) = TTCMIN;
        chromosome(i,V+4) = MinDistPedestrianAwa;
    end
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% 4. NSGAII: Sort the initial population. %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    K=V+1;
    chromosome = non_domination_sort_mod(chromosome, M, K);
    A=chromosome;
    
    
    formatOut = 'yyyymmdd_HHMMss_FFF';
    
    ds=datestr(now,formatOut);
    name1 = strcat('NSGAIISMResults_',ds,'.txt');
    
    
    fid = fopen(name1, 'w');
    fprintf(fid, '\n initial chromosome\n');
    fprintf(fid, '%s %s  %s  %s  %s  %s  %s  %s  %s  %s  %s %s \n',['ind' '    ' 'x0P' '     ' 'y0P' '     ' 'Th0P' '     ' 'v0P' '     ' 'v0C' '   ' 'OF1' '    ' 'OF2' '   ' 'OF3' '  ' 'Rank' '  ' 'CD' '  ' '10:Exact 99:Predict']);
    fprintf(fid, '\n');
    clear EC
    EC(:,1:M+V+3)=chromosome;
    EC(:,M+V+4)=10;
    clear a;
    for i=1:size(EC,1)
        a(:,i)=EC(i,:);
    end
    fprintf(fid, '%d %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f  %d %.8f  %d \n', a);
    
    
    NCSIM=0;
    
    SimTimeUntilNow=toc
    gim=0;
    
    while SimTimeUntilNow <9000 %150min
        A=[];
        Exact=chromosome(:,1:M+V+1);
        
        gim=gim+1;
        fprintf(fid, 'number of times we call the simulation until now = %d \n', NCSIM);
        fprintf(fid, '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \n');
        fprintf(fid, 'Generation Number = %d \n',gim);
        
        t1NewSol=rem(now,1);
        timeSimUntilNow=0;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 5. NSGAII: Select mates using tournament selection %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        pool = round(pop/2);
        tour = 2;
        
        parent_chromosome = tournament_selection(chromosome, pool, tour);
        
        
        mu = 20;
        mum = 20;
        
        [N,m] = size(parent_chromosome);
        clear m
        p = 1;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 6. NSGAII: Perform crossover and mutation %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        for i = 1 : N
            % With 90 % probability perform crossover
            if rand(1) < 0.9
                % Initialize the children to be null vector.
                child_1 = [];
                child_2 = [];
                % Select the first parent
                parent_1 = round(N*rand(1));
                if parent_1 < 1
                    parent_1 = 1;
                end
                % Select the second parent
                parent_2 = round(N*rand(1));
                if parent_2 < 1
                    parent_2 = 1;
                end
                % Make sure both the parents are not the same.
                while isequal(parent_chromosome(parent_1,:),parent_chromosome(parent_2,:))
                    parent_2 = round(N*rand(1));
                    if parent_2 < 1
                        parent_2 = 1;
                    end
                end
                
                % Get the chromosome information for each randomnly selected
                % parents
                parent_1 = parent_chromosome(parent_1,:);
                parent_2 = parent_chromosome(parent_2,:);
                child_1(1)=40+i;
                child_2(1)=50+i;
                % Perform corssover for each decision variable in the chromosome.
                for j = 2 : V+1
                    % SBX (Simulated Binary Crossover).
                    % For more information about SBX refer the enclosed pdf file.
                    % Generate a random number
                    u(j) = rand(1);
                    if u(j) <= 0.5
                        bq(j) = (2*u(j))^(1/(mu+1));
                    else
                        bq(j) = (1/(2*(1 - u(j))))^(1/(mu+1));
                    end
                    % Generate the jth element of first child
                    child_1(j) = ...
                        0.5*(((1 + bq(j))*parent_1(j)) + (1 - bq(j))*parent_2(j));
                    % Generate the jth element of second child
                    child_2(j) = ...
                        0.5*(((1 - bq(j))*parent_1(j)) + (1 + bq(j))*parent_2(j));
                    % Make sure that the generated element is within the specified
                    % decision space else set it to the appropriate extrema.
                    if child_1(j) > max_range(j)
                        child_1(j) = max_range(j);
                    elseif child_1(j) < min_range(j)
                        child_1(j) = min_range(j);
                    end
                    if child_2(j) > max_range(j)
                        child_2(j) = max_range(j);
                    elseif child_2(j) < min_range(j)
                        child_2(j) = min_range(j);
                    end
                end

                
            else
                
                % Initialize the children to be null vector.
                child_1 = [];
                child_2 = [];
                % Select the first parent
                parent_1 = round(N*rand(1));
                if parent_1 < 1
                    parent_1 = 1;
                end
                % Select the second parent
                parent_2 = round(N*rand(1));
                if parent_2 < 1
                    parent_2 = 1;
                end
                % Make sure both the parents are not the same.
                while isequal(parent_chromosome(parent_1,:),parent_chromosome(parent_2,:))
                    parent_2 = round(N*rand(1));
                    if parent_2 < 1
                        parent_2 = 1;
                    end
                end
                % Get the chromosome information for each randomnly selected
                % parents
                parent_1 = parent_chromosome(parent_1,:);
                parent_2 = parent_chromosome(parent_2,:);
                child_1(1)=40+i;
                child_2(1)=50+i;
                for j = 2 : V+1
                    child_1(j)=parent_1(j);
                    
                    child_2(j)=parent_2(j);
                end
                
            end
            
            if rand(1) < 0.5
                %do mutation
                delta=[0 2 2 10 1 1.4];
                for j = 2 : V+1
                    
                    child_1(j) = child_1(j) + Fn_MiLTester_My_Normal_Rnd(0,delta(j));
                    % Make sure that the generated element is within the decision
                    % space.
                    if child_1(j) > max_range(j)
                        child_1(j) = max_range(j);
                    elseif child_1(j) < min_range(j)
                        child_1(j) = min_range(j);
                    end
                    
                    child_2(j) = child_2(j) + Fn_MiLTester_My_Normal_Rnd(0,delta(j));
                    % Make sure that the generated element is within the decision
                    % space.
                    if child_2(j) > max_range(j)
                        child_2(j) = max_range(j);
                    elseif child_2(j) < min_range(j)
                        child_2(j) = min_range(j);
                    end
                end
            else
                %copy
                for j = 2 : V+1
                    child_1(j)=child_1(j);
                    
                    child_2(j)=child_2(j);
                end
            end
            
            
            x0P=child_1(2);
            y0P=child_1(3);
            ThP=child_1(4);
            v0P=child_1(5);
            v0C=child_1(6);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% 7. NSGAII-SM: Use surrogate model to evaluate the objective functions for child 1. %%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%          
            ch1=[x0P; y0P;ThP;v0P;v0C];
            [of1P, of1Pmin, of1Pmax]=surrogateModelOF1AverageBestR2(ch1);
            [of2P, of2Pmin, of2Pmax]=surrogateModelOF2AverageBestR2(ch1);
            [of3P, of3Pmin, of3Pmax]=surrogateModelOF3AverageBestR2(ch1);
            child_1P=child_1;
            child_1P(:,V+2)= of1P;
            child_1P(:,V+3) = of2P;
            child_1P(:,V+4) = of3P;
            
            
            child_1W=child_1;
            child_1W(:,V+2) = of1Pmax;
            child_1W(:,V+3) = of2Pmax;
            child_1W(:,V+4) = of3Pmax;
            
            child_1B=child_1;
            child_1B(:,V+2) = of1Pmin;
            child_1B(:,V+3) = of2Pmin;
            child_1B(:,V+4) = of3Pmin;
            
            
            sum1 = 0;
            sum2 = 0;
            sum3 = 0;
            
            x0P=child_2(2);
            y0P=child_2(3);
            ThP=child_2(4);
            v0P=child_2(5);
            v0C=child_2(6);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% 8. NSGAII-SM: Use surrogate model to evaluate the objective functions for child 2. %%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%              
            ch2=[x0P; y0P;ThP;v0P;v0C];
            
            [of1P, of1Pmin, of1Pmax]=surrogateModelOF1AverageBestR2(ch2);
            [of2P, of2Pmin, of2Pmax]=surrogateModelOF2AverageBestR2(ch2);
            [of3P, of3Pmin, of3Pmax]=surrogateModelOF3AverageBestR2(ch2);
            child_2P=child_2;
            child_2P(:,V+2)= of1P;
            child_2P(:,V+3) = of2P;
            child_2P(:,V+4) = of3P;
            
            
            child_2W=child_2;
            child_2W(:,V+2) = of1Pmax;
            child_2W(:,V+3) = of2Pmax;
            child_2W(:,V+4) = of3Pmax;
            
            child_2B=child_2;
            child_2B(:,V+2) = of1Pmin;
            child_2B(:,V+3) = of2Pmin;
            child_2B(:,V+4) = of3Pmin;
            
            
            % Keep proper count and appropriately fill the child variable with all
            % the generated children for the particular generation.
            
            childP(p,:) = child_1P;
            childP(p+1,:) = child_2P;
            childW(p,:) = child_1W;
            childW(p+1,:) = child_2W;
            childB(p,:) = child_1B;
            childB(p+1,:) = child_2B;
            was_cossover = 0;
            p = p + 2;
            
        end
        offspring_chromosomeP = childP;
        offspring_chromosomeW = childW;
        offspring_chromosomeB = childB;
        
        %
        
        [main_pop,temp] = size(chromosome);
        [offspring_pop,temp] = size(offspring_chromosomeP);
        clear temp
        intermediate_chromosomeP(1:main_pop,:) = chromosome;
        intermediate_chromosomeP(main_pop + 1 : main_pop + offspring_pop,1 : M+V+1) = ...
            offspring_chromosomeP;
        
        intermediate_chromosomeW(1:main_pop,:) = chromosome;
        intermediate_chromosomeW(main_pop + 1 : main_pop + offspring_pop,1 : M+V+1) = ...
            offspring_chromosomeW;
        
        intermediate_chromosomeB(1:main_pop,:) = chromosome;
        intermediate_chromosomeB(main_pop + 1 : main_pop + offspring_pop,1 : M+V+1) = ...
            offspring_chromosomeB;
        
        intermediate_chromosomeP = ...
            non_domination_sort_mod(intermediate_chromosomeP, M, V+1);
        
        intermediate_chromosomeW = ...
            non_domination_sort_mod(intermediate_chromosomeW, M, V+1);
        
        intermediate_chromosomeB = ...
            non_domination_sort_mod(intermediate_chromosomeB, M, V+1);
        
        clear Exact
        clear InP
        clear InB
        clear InW
        clear saveFile
        clear EC
        Exact=chromosome(:,1:M+V+1);
        
        EC(:,1:M+V+3)=chromosome;
        EC(:,M+V+4)=10;
        
        InP(:,1:M+V+3)= intermediate_chromosomeP;
        
        for g=1:size(InP,1)
            verify = isItExact(InP(g,:), Exact);
            
            if verify == true
                InP(g,M+V+4)=10;
            else
                InP(g,M+V+4)=99;
            end
        end
        
        InW(:,1:M+V+3)= intermediate_chromosomeW;
        
        for g=1:size(InW,1)
            verify = isItExact(InW(g,:), Exact);
            
            if verify == true
                InW(g,M+V+4)=10;
            else
                InW(g,M+V+4)=99;
            end
        end
        
        
        InB(:,1:M+V+3)= intermediate_chromosomeB;
        
        for g=1:size(InB,1)
            verify = isItExact(InB(g,:), Exact);
            
            if verify == true
                InB(g,M+V+4)=10;
            else
                InB(g,M+V+4)=99;
            end
        end
        
        [main_popC] = size(EC,1);
        [offspring_popP] = size(InP,1);
        [offspring_popW] = size(InW,1);
        [offspring_popB] = size(InB,1);
        
        saveFile(1:main_popC,:)=EC;
        saveFile(1+main_popC:main_popC+offspring_popP ,:)=InP;
        saveFile(1+main_popC+offspring_popP: main_popC+offspring_popP + offspring_popW,:)=InW;
        saveFile(1+main_popC+offspring_popP + offspring_popW: main_popC+offspring_popP + offspring_popW + offspring_popB,:)=InB;
        
        fprintf(fid, '\n chromosome\n');
        fprintf(fid, '%s  %s %s  %s  %s  %s  %s  %s  %s  %s  %s %s \n',['index' '  ' 'x0P' '     ' 'y0P' '     ' 'Th0P' '     ' 'v0P' '     ' 'v0C' '   ' 'OF1' '    ' 'OF2' '   ' 'OF3' '  ' 'Rank' '  ' 'CD' '  ' '10:Exact 99:Predict']);
        fprintf(fid, '\n');
        
        
        
        clear a;
        for i=1:size(EC,1)
            a(:,i)=EC(i,:);
        end
        fprintf(fid, '%d %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f  %d %.8f  %d \n', a);
        fprintf(fid, 'R^\n');
        clear a;
        for i=1:size(InP,1)
            a(:,i)=InP(i,:);
        end
        fprintf(fid, '%d %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f  %d %.8f  %d \n', a);
        fprintf(fid, 'R^+e\n');
        clear a;
        for i=1:size(InW,1)
            a(:,i)=InW(i,:);
        end
        fprintf(fid, '%d %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f  %d %.8f  %d \n', a);
        fprintf(fid, 'R^-e\n');
        clear a;
        for i=1:size(InB,1)
            a(:,i)=InB(i,:);
        end
        fprintf(fid, '%d %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f  %d %.8f  %d \n', a);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 9. NSGAII-SM: Select the best individuals. %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
        
        A=[];
        B = false;
        
        cc = true;
        ntimeCallsimIn1A=0;
      
        while size(A,1) < pop
            
            
            % Find the maximum rank in the current population
            max_rankB = maxRank(intermediate_chromosomeB(:,M + V + 2));
            
            clear IBConcat
            
            IBConcat=[];
            Attend=0;
            for rankB=1:max_rankB
                if Attend == 1
                    break;
                end
                clear IB
                clear IECD
                IECD=[];
                intermediate_chromosomeBR1 = findElementinRankx (intermediate_chromosomeB, rankB, M, V+1);
                intermediate_chromosomeBR1Exact = findElementIntersection(intermediate_chromosomeBR1, Exact, M, V+1);%find element that are in chromosome and rank 1 in intermediate_chromosomeB  R1^- and Exact
                if size(intermediate_chromosomeBR1Exact,1)>=1
                    cc=true;
                    fprintf(fid, '\n partI is satisfied \n');
                    if size(intermediate_chromosomeBR1Exact) == size(intermediate_chromosomeBR1)
                        if size(A,1)+size(intermediate_chromosomeBR1Exact,1) >= pop
                            IB = findElementHighestCD(intermediate_chromosomeBR1Exact, pop - size(A,1), M, V+1);%select the pop - size(A) highest CD
                            B = true;
                            Attend = 1;
                        else
                            IB=intermediate_chromosomeBR1Exact;
                        end
                        
                        
                    else
                        intermediate_chromosomeBR1Predict = findElementIntersection(intermediate_chromosomeBR1, offspring_chromosomeB, M, V+1);
                        [P,cd]=highestCD(intermediate_chromosomeBR1Predict, M, V+1);
                        IECD=selectExactHigherthanCDP(intermediate_chromosomeBR1Exact, cd, M,V+1);
                        
                        if size(IECD,1)>0
                            
                            if size(A,1)+size(IECD,1) >= pop
                                IB = findElementHighestCD(IECD, pop - size(A,1), M, V+1);%select the pop - size(A) highest CD
                                B = true;
                            else
                                IB=IECD;
                                
                            end
                            
                        else
                            
                            fprintf(fid, '\n we do not select any element from part I!\n');
                            break;
                        end
                        
                        
                        Attend= 1;
                    end
                    %%ADD element to A
                    
                    if size(A,1)>=1
                        A(1:size(A,1),1:M+V+1)=A;
                        A(size(A,1)+1:size(A,1) + size(IB,1),1:M+V+1)=IB(:,1:M+V+1);  %A<--A union IB
                        if size(A) > 10
                            display('A>10 error');
                            break;
                        end
                    else %if size(IB,1)>=1
                        A(1:size(IB,1),1:M+V+1)=IB(:,1:M+V+1);
                    end
                    for f=1:size(A,1)
                        if A(f,1)==0
                            tt=0;
                        end
                    end
                    
                    fprintf(fid, '\n A\n');
                    clear a;
                    for i=1:size(A,1)
                        a(:,i)=A(i,:);
                    end
                    fprintf(fid, '%d%.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f \n', a);
                    
                    
                    intermediate_chromosomeP = deleteElement (intermediate_chromosomeP, IB, M, V+1);%P<-P/IB
                    intermediate_chromosomeB = deleteElement (intermediate_chromosomeB, IB, M, V+1);%P<-P/IB
                    intermediate_chromosomeW = deleteElement (intermediate_chromosomeW, IB, M, V+1);%P<-P/IB
                    
                    clear b;
                    b=size(A,1);
                    clear c
                    c=size(IB,1);
                    fprintf(fid, '\n A <-- %d', c);
                    fprintf(fid, ' new elements \n');
                    fprintf(fid, 'size (A) = %d \n',b);
                    
                    
                    
                    
                else
                    cc=false;
                    break;
                end
                if B == true
                    break;
                end
            end
            
            
            if B==true
                break;
            else
                
                % Find the maximum rank in the current population
                max_rankW = maxRank(intermediate_chromosomeW(:,M + V + 2));
                clear IBConcat
                for rankW=1:max_rankW
                    clear IW
                    intermediate_chromosomeWR1 = findElementinRankx (intermediate_chromosomeW, rankW, M, V+1);
                    intermediate_chromosomeWR1Predicted = findElementIntersection(intermediate_chromosomeWR1, offspring_chromosomeW, M, V+1);
                    
                    
                    if size(intermediate_chromosomeWR1Predicted,1)>=1
                        fprintf(fid, '\n partII is satisfied \n');
                        
                        if size(intermediate_chromosomeWR1Predicted,1)>1
                            [PP,cdP]=highestCDwithLowRankinB(intermediate_chromosomeWR1Predicted,intermediate_chromosomeB, M, V+1);
                            if size(A,1)+1>=pop
                                B = true;
                            end
                            IW=PP;
                        else
                            IW=intermediate_chromosomeWR1Predicted;
                            if size(A,1)+1>=pop
                                B = true;
                            end
                        end
                        fprintf(fid, '\n call the simulation \n');
                        ntimeCallsimIn1A=ntimeCallsimIn1A+1;
                        NCSIM=NCSIM+1;
                        Fct(1,1:M+V+3)= findIinIW(IW(1,:),intermediate_chromosomeW);
                        Fct(1,M+V+4)=99;
                        
                        Fct(2,1:M+V+3)=findIinIW(IW(1,:),intermediate_chromosomeP);
                        Fct(2,M+V+4)=99;
                        
                        Fct(3,1:M+V+3)= findIinIW(IW(1,:),intermediate_chromosomeB);
                        Fct(3,M+V+4)=99;
                        
                        
                        
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        %%% 10. Prescan: Run a simulation for the selected predicted elements. %%%
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
                        x0P=IW(1,2);
                        y0P=IW(1,3);
                        ThP=IW(1,4);
                        v0P=IW(1,5);
                        v0C=IW(1,6);

                        BestDist=60;
                        MinDistPedestrianCar=100;
                        TTCMIN=4;
                        MinDistPedestrianAwa=50;
                        
                        %%%% Change position and orientation of Pedestrian
                        %%%%%%%%% Generate the experiment %%%%%%%%%
                        disp('Variables reinitialized...');
                        disp('------------------------');
                        startTime = rem(now,1);
                        startTimeA = rem(now,1);
                        Experiment_Name='Experiment_2'; % Hereby enter your experiment name
                        PreScan_ExperimentName= [Experiment_Name '.pex']; % add the extension for the PreScan file
                        Simulink_ModelName= [ Experiment_Name '_cs'];% add the extension for the Simulink file
                        PreScan_ExperimentLocation = [pwd '\' PreScan_ExperimentName];
                        simulationCount=0;
                        k=0;
                        A1 = cell(3,1);
                        B1 = cell(3,1);
                        NbrOfTagAndVal=0;
                        
                        
                        NbrOfTagAndVal=NbrOfTagAndVal+1;
                        A1{NbrOfTagAndVal}='x_Person';
                        radiusvalue={num2str(x0P)};
                        idx = randi(numel(radiusvalue),1) ;
                        currentName = radiusvalue{idx};
                        B1{NbrOfTagAndVal}=currentName;
                        
                        
                        NbrOfTagAndVal=NbrOfTagAndVal+1;
                        A1{NbrOfTagAndVal}='y_Person';
                        radiusvalue={num2str(y0P)};
                        idx = randi(numel(radiusvalue),1) ;
                        currentName = radiusvalue{idx};
                        B1{NbrOfTagAndVal}=currentName;
                        
                        NbrOfTagAndVal=NbrOfTagAndVal+1;
                        A1{NbrOfTagAndVal}='Th_Person';
                        radiusvalue={num2str(ThP)};
                        idx = randi(numel(radiusvalue),1) ;
                        currentName = radiusvalue{idx};
                        B1{NbrOfTagAndVal}=currentName;
                        
                        
                        for m=1:NbrOfTagAndVal
                            clear Tag Val
                            Tag(m,:) =A1{m};
                            Val(m,:)=B1{m};
                            dos( ['PreScan.CLI.exe -convertAndLoad ' PreScan_ExperimentLocation ' -set ' Tag(m,:) '=' Val(m,:)]);     % Change the value in PreScan
                            dos( 'PreScan.CLI.exe -save');         % Save the PreScan experiment
                            
                        end
                        
                        dos ('PreScan.CLI.exe --realignPaths');
                        dos( 'PreScan.CLI.exe -build');
                        
                        open_system(Simulink_ModelName);                 % Open the Simulink Model
                        
                        regenButtonHandle = find_system(Simulink_ModelName, 'FindAll', 'on', 'type', 'annotation','text','Regenerate');
                        regenButtonCallbackText = get_param(regenButtonHandle,'ClickFcn');
                        
                        eval(regenButtonCallbackText);
                        
                        %%%%
                        %Run Simulation
                        %***
                        sim(fullfile(mfilepath,'/Experiment_2_cs.mdl'));
                        %***
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        %%% 11. NSGAII-SM: Evaluate the objective functions for those elements. %%%
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        AA=SimStopTime.time;
                        b=numel(AA);
                        TotSim=AA(b);
                        [MinDistPedestrianCar,TTCMIN,MinDistPedestrianAwa]=ObjectiveFunctionsDistancesNew(TotSim,SimulationTimeStep, xCar,yCar,vCar,xPerson,yPerson,vPerson,ThP,TTCcol)
                        
                        NFIT=NFIT+1;
                        IW(1,V+2)= MinDistPedestrianCar;
                        IW(1,V+3) = TTCMIN;
                        IW(1,V+4) = MinDistPedestrianAwa;
                        
                        Fct(4,1:M+V+3)= IW(1,:);
                        Fct(4,M+V+4)=10;
                        
                        
                        fprintf(fid, '[F^+e, F^, F^-e, F] \n');
                        clear a;
                        for i=1:size(Fct,1)
                            a(:,i)=Fct(i,:);
                        end
                        fprintf(fid, '%d%.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f  %d %.8f %d \n', a);
                        
                        
                        %end
                        fprintf(fid, '\n Total number of times we call the sim in A = %d \n', ntimeCallsimIn1A);
                        break;
                    else
                        t=1;
                    end
                end
                
                
                if size(intermediate_chromosomeWR1Predicted,1)>=1
                    
                    %Exact<--exact union IW
                    Exact(1:size(Exact,1),1:M+V+1)=Exact;
                    %if size(IW,1)>=1
                    Exact(size(Exact,1)+1:size(Exact,1)+ size(IW,1),1:M+V+1)=IW(1,1:M+V+1);
                    
                    
                    %P<-- P/IW
                    intermediate_chromosomeP = replaceOneElement (intermediate_chromosomeP, IW, M, V+1);%P<-P/IW
                    intermediate_chromosomeB = replaceOneElement (intermediate_chromosomeB, IW, M, V+1);%P<-P/IW
                    intermediate_chromosomeW = replaceOneElement (intermediate_chromosomeW, IW, M, V+1);%P<-P/IW
                    
                    %Predicted <--predicted/IW
                    offspring_chromosomeW = deleteOneElement (offspring_chromosomeW, IW, M, V+1);
                    offspring_chromosomeB = deleteOneElement (offspring_chromosomeB, IW, M, V+1);
                    offspring_chromosomeP = deleteOneElement (offspring_chromosomeP, IW, M, V+1);
                    
                end
                
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% 12. NSGAII-SM: Sort the population. %%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            intermediate_chromosomeP = ...
                non_domination_sort_mod(intermediate_chromosomeP, M, V+1);
            
            intermediate_chromosomeW = ...
                non_domination_sort_mod(intermediate_chromosomeW, M, V+1);
            
            intermediate_chromosomeB = ...
                non_domination_sort_mod(intermediate_chromosomeB, M, V+1);
            
            fprintf(fid, '\n updateP \n');
            
            clear InP
            clear InW
            clear InB
            clear saveFile
            InP(:,1:M+V+3)= intermediate_chromosomeP;
            
            for g=1:size(InP,1)
                verify = isItExact(InP(g,:), Exact);
                
                if verify == true
                    InP(g,M+V+4)=10;
                else
                    InP(g,M+V+4)=99;
                end
            end
            
            InW(:,1:M+V+3)= intermediate_chromosomeW;
            
            for g=1:size(InW,1)
                verify = isItExact(InW(g,:), Exact);
                
                if verify == true
                    InW(g,M+V+4)=10;
                else
                    InW(g,M+V+4)=99;
                end
            end
            
            
            InB(:,1:M+V+3)= intermediate_chromosomeB;
            
            for g=1:size(InB,1)
                verify = isItExact(InB(g,:), Exact);
                
                if verify == true
                    InB(g,M+V+4)=10;
                else
                    InB(g,M+V+4)=99;
                end
            end
            
            [offspring_popP] = size(InP,1);
            [offspring_popW] = size(InW,1);
            [offspring_popB] = size(InB,1);
            
            saveFile(1:offspring_popP ,:)=InP;
            saveFile(1+offspring_popP: offspring_popP + offspring_popW,:)=InW;
            saveFile(1+offspring_popP + offspring_popW: offspring_popP + offspring_popW + offspring_popB,:)=InB;
            
            fprintf(fid, '\n R^\n');
            clear a;
            for i=1:size(InP,1)
                a(:,i)=InP(i,:);
            end
            fprintf(fid, '%d%.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f  %d %.8f %d \n', a);
            fprintf(fid, 'R^+e\n');
            clear a;
            for i=1:size(InW,1)
                a(:,i)=InW(i,:);
            end
            fprintf(fid, '%d%.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f  %d %.8f %d \n', a);
            fprintf(fid, 'R^-e\n');
            clear a;
            for i=1:size(InB,1)
                a(:,i)=InB(i,:);
            end
            fprintf(fid, '%d%.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f  %d %.8f %d \n', a);
            
            
            
        end
        SimTimeUntilNow=toc
        fprintf(fid, 'SimTimeUntilNow%.8f \n', SimTimeUntilNow);
        
        chromosome=A;
        for i=1:10
            chromosome(i,1)=30+i;
        end
        chromosome = non_domination_sort_mod(chromosome, M, V+1);
        
        
        fprintf(fid, '\n chromosome \n');
        clear a;
        for i=1:size(chromosome,1)
            a(:,i)=chromosome(i,:);
        end
        fprintf(fid, '%d%.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f  %d %.8f \n', a);
        

        fprintf(fid, '\n Total number of times we call the sim in A = %d \n', ntimeCallsimIn1A);
        fprintf(fid, 'SimTimeUntilNow%.8f \n', SimTimeUntilNow);
        
        if ~mod(i,100)
            clc
            fprintf('%d generations completed\n',i);
        end
        
        InC(:,1:M+V+3)= chromosome;
        
        
        SimTimeUntilNow=toc
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Print NSGAII-SM results %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    
    fprintf(fid, '\n solution \n');
    clear a;
    for i=1:size(chromosome,1)
        a(:,i)=chromosome(i,:);
    end
    fprintf(fid, '%d%.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f  %d %.8f \n', a);
    
    
    
    fprintf(fid, '\n Total number of times we call the sim in 400mn = %d \n', NCSIM);
    fprintf(fid, 'totSimTime =%.8f \n', toc);
    
    
    fclose(fid);
    
catch exc
    display(getReport(exc));
    display('Error in model test run!');
end