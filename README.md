# Testing Advanced Driver Assistance Systems (TestingADAS)

[Overview](#overview)

[Installation and Setup](#Installation-and-Setup)

[License and Copyright](#License-and-Copyright)

[Related Publication](#Related-Publication)

---
# Overview
TestingADAS is a tool for testing Advanced Driver Assistance Systems using Multi-objective Search and Neural Networks. 
We use multi-objective search to guide testing towards the most critical behaviors of ADAS. 
Neural Network enables our testing approach to explore a larger part of the input search space within limited computational resources.

- _runNSGAIIASE12.m_ executes the NSGAII algorithm and produces test scenarios that stress several critical aspects of the system and the environment at the same time.
- _runNSGAIISMASE12.m_ executes the NSGAII-SM algorithm and produces critical test scenarios under a limited and realistic time budget. 

---
# Installation and Setup
The tool is used with a simulation environment called [PreScan](https://tass.plm.automation.siemens.com/prescan). The software under test is developed in Simulink and is integrated into PreScan using inter-block connections 
between the Simulink models of the car and the software under test. 

Note that this code can be used with any simulation environment. 

---
# License and Copyright
TestingADAS is copyrighted © by University of Luxembourg / Interdisciplinary Centre for Security, Reliability and Trust - 2015-2019.

Acknowledged co-authors:

- Raja Ben Abdessalem (benabdessalem@svv.lu)
- Shiva Nejati (nejati@svv.lu)
- Briand Lionel (briand@svv.lu)
- Thomas Stifter (Thomas.Stifter@iee.lu)

---
# Related Publication
(All Author's preprint version of the publications are available at https://dblp.uni-trier.de/pers/hd/a/Abdessalem:Raja_Ben)

** Testing Advanced Driver Assistance Systems using Multi-objective Search and Neural Networks**  
*Raja Ben Abdessalem, Shiva Nejati, Lionel Briand, and Thomas Stifter, in the proceedings of the 31st IEEE/ACM International Conference on Automated Software Engineering, ASE 2016, Singapore, September 3-7, 2016.*
