% % %
%  Copyright (c) {2015-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
%  Raja Ben Abdessalem (benabdessalem@svv.lu)
%  Shiva Nejati (nejati@svv.lu)
%  Briand Lionel (briand@svv.lu)
%  Thomas Stifter (Thomas.Stifter@iee.lu)
%  All rights reserved.
%
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions are
%  met:
%
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
%
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%  POSSIBILITY OF SUCH DAMAGE.


try
    %%%%%%%%%%%%%%%%%%%%%%
    %%% INITIALIZATION %%%
    %%%%%%%%%%%%%%%%%%%%%%
    mfilepath=fileparts(which('runNSGAIIASE16.m'));
    addpath(fullfile(mfilepath,'/Functions/Functions'));
    addpath(fullfile(mfilepath,'/GA'));
    load_system(fullfile(mfilepath,'/Experiment_2_cs.mdl'));
    tic
    
    ST=10;
    Fn_MiLTester_SetSimulationTime(ST);
    SimulationTimeStep=GetSimulationTimeStep();
    startTime = rem(now,1);
    modelRunningTime=0;
    chromosome=[];
    
    pop=10; %population size
    
    % initalize search parameters
    
    M=3;%nbr_obj_funcs
    V=5;%nbr_inputs 
    K = M + V;
    
    %x0C=0;
    %y0C=50;
    min=[20;35;40;1;10];
    max=[85; 48;160;5;25];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% 1. NSGAII: Randomly create an initial population. %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
    
    NFIT=0;
    for i = 1 : pop
        numpop=i;
        display(numpop);
        AA=[];
        b=0;
        TotSim=10;
        for j = 1 : V
            chromosome(i,j) = (min(j) + (max(j) - min(j))*rand(1));
        end
        
        
        x0P=chromosome(i,1)
        y0P=chromosome(i,2)
        ThP=chromosome(i,3)
        v0P=chromosome(i,4)
        v0C=chromosome(i,5)
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 2. Prescan: Run simulations for the initial population. %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
       
    
        MinDistPedestrianCar=100;
        TTCMIN=4;
        MinDistPedestrianAwa=50;
        MaxD=0;
        Det=0;
        %***
        %%%% Change position and orientation of Pedestrian
        %%%%%%%%% Generate the experiment %%%%%%%%%
        disp('Variables reinitialized...');
        disp('------------------------');
        startTime = rem(now,1);
        startTimeA = rem(now,1);
        Experiment_Name='Experiment_2'; % Hereby enter your experiment name
        PreScan_ExperimentName= [Experiment_Name '.pex']; % add the extension for the PreScan file
        Simulink_ModelName= [ Experiment_Name '_cs'];% add the extension for the Simulink file
        PreScan_ExperimentLocation = [pwd '\' PreScan_ExperimentName];
        simulationCount=0;
        k=0;
        A = cell(3,1);
        B = cell(3,1);
        NbrOfTagAndVal=0;
        
        
        NbrOfTagAndVal=NbrOfTagAndVal+1;
        A{NbrOfTagAndVal}='x_Person';
        radiusvalue={num2str(x0P)};
        idx = randi(numel(radiusvalue),1) ;
        currentName = radiusvalue{idx};
        B{NbrOfTagAndVal}=currentName;
        
        
        NbrOfTagAndVal=NbrOfTagAndVal+1;
        A{NbrOfTagAndVal}='y_Person';
        radiusvalue={num2str(y0P)};
        idx = randi(numel(radiusvalue),1) ;
        currentName = radiusvalue{idx};
        B{NbrOfTagAndVal}=currentName;
        
        NbrOfTagAndVal=NbrOfTagAndVal+1;
        A{NbrOfTagAndVal}='Th_Person';
        radiusvalue={num2str(ThP)};
        idx = randi(numel(radiusvalue),1) ;
        currentName = radiusvalue{idx};
        B{NbrOfTagAndVal}=currentName;
        
        
        for m=1:NbrOfTagAndVal
            clear Tag Val
            Tag(m,:) =A{m};
            Val(m,:)=B{m};
            dos( ['PreScan.CLI.exe -convertAndLoad ' PreScan_ExperimentLocation ' -set ' Tag(m,:) '=' Val(m,:)]);     % Change the value in PreScan
            dos( 'PreScan.CLI.exe -save');         % Save the PreScan experiment
            
        end
        
        dos ('PreScan.CLI.exe --realignPaths');
        dos( 'PreScan.CLI.exe -build');
        
        open_system(Simulink_ModelName);                 % Open the Simulink Model
        
        regenButtonHandle = find_system(Simulink_ModelName, 'FindAll', 'on', 'type', 'annotation','text','Regenerate');
        regenButtonCallbackText = get_param(regenButtonHandle,'ClickFcn');
        
        eval(regenButtonCallbackText);
        
        %%%%
        %Run Simulation
        
        
        sim(fullfile(mfilepath,'/Experiment_2_cs.mdl'));
        maxSimulationsPerViewer=5;
        closeViewer = mod(simulationCount + 1, maxSimulationsPerViewer) == 0; % Close viewer is true if Simulation is > than maxSimulationsPerViewer
        simulationCount = simulationCount + 1;
        if (closeViewer)
            visViewer = 5;
            maxWaitTimeMS = 10*1000;
            nl.tno.wt.prescan.utils.ProcessManagement.Stop(visViewer, maxWaitTimeMS);
            pause(0.5);
            nl.tno.wt.prescan.utils.ProcessManagement.Start(5, maxWaitTimeMS);
        end        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 3. NSGAII: Evaluate the objective functions for the initial population. %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
        
        for w=1:length(Detection.signals.values)
            if Detection.signals.values(w)>MaxD
                MaxD=Detection.signals.values(w);
            end
        end
        if MaxD~=0
            Det=1;
        end
        
        chromosome(i,V+1)=Det;

        AA=SimStopTime.time;
        b=numel(AA);
        TotSim=AA(b);
        [MinDistPedestrianCar,TTCMIN,MinDistPedestrianAwa]=ObjectiveFunctionsDistancesNew(TotSim,SimulationTimeStep, xCar,yCar,vCar,xPerson,yPerson,vPerson,ThP,TTCcol);
        
        NFIT=NFIT+1;
       
        
        
        chromosome(i,V+2)= MinDistPedestrianCar;
        chromosome(i,V+3) = TTCMIN;
        chromosome(i,V+4) = MinDistPedestrianAwa;
        
        
        
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% 4. NSGAII: Sort the initial population. %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    chromosome = non_domination_sort_mod(chromosome, M, V+1);
    
    formatOut = 'yyyymmdd_HHMMss_FFF';
    
    ds=datestr(now,formatOut);
    name1 = strcat('NSGAIIResults_',ds,'.txt');    
    fid = fopen(name1, 'w');
    fprintf(fid, '\n initial chromosome\n');
    fprintf(fid, '%s  %s  %s  %s  %s  %s  %s  %s  %s  %s  %s\n',['x0P' '       ' 'y0P' '        ' 'Th0P' '       ' 'v0P' '      ' 'v0C' '     ' 'Det' '   ' 'OF1' '    ' 'OF2' '   ' 'OF3'  '     ' 'Rank' '    ' 'CD']);
    fprintf(fid, '\n');
    EC(:,1:M+V+3)=chromosome;
    clear a;
    for i=1:size(EC,1)
        a(:,i)=EC(i,:);
    end
    fprintf(fid, '%.6f  %.6f  %.6f  %.6f  %.6f  %d  %.6f  %.6f  %.6f  %d %.6f \n', a);
    NCSIM=0;
    
    
    
    SimTimeUntilNow=toc
    gim=0;
    
    while SimTimeUntilNow <=9000 %150 min
        SimTimeUntilNow=toc
        fprintf(fid, 'SimTimeUntilNow %.3f \n', SimTimeUntilNow);
        gim=gim+1;
        fprintf(fid, '\n Total number of times we call the sim until now = %d \n', NCSIM);
        fprintf(fid, '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \n');
        fprintf(fid, 'Generation Number = %d \n',gim);
        
               
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 5. NSGAII: Select mates using tournament selection %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        pool = round(pop/2);
        tour = 2;
        
        parent_chromosome = tournament_selection(chromosome, pool, tour);
        
        
        mu = 20;
        mum = 20;
        
        [N,m] = size(parent_chromosome);
        clear m
        p = 1;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 6. NSGAII: Perform crossover and mutation %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        for i = 1 : N
            % With 90 % probability perform crossover
            if rand(1) < 0.9
                % Initialize the children to be null vector.
                child_1 = [];
                child_2 = [];
                % Select the first parent
                parent_1 = round(N*rand(1));
                if parent_1 < 1
                    parent_1 = 1;
                end
                % Select the second parent
                parent_2 = round(N*rand(1));
                if parent_2 < 1
                    parent_2 = 1;
                end
                % Make sure both the parents are not the same.
                while isequal(parent_chromosome(parent_1,:),parent_chromosome(parent_2,:))
                    parent_2 = round(N*rand(1));
                    if parent_2 < 1
                        parent_2 = 1;
                    end
                end
                % Get the chromosome information for each randomnly selected
                % parents
                parent_1 = parent_chromosome(parent_1,:);
                parent_2 = parent_chromosome(parent_2,:);
                % Perform corssover for each decision variable in the chromosome.
                for j = 1 : V
                    % SBX (Simulated Binary Crossover).
                    % For more information about SBX refer the enclosed pdf file.
                    % Generate a random number
                    u(j) = rand(1);
                    if u(j) <= 0.5
                        bq(j) = (2*u(j))^(1/(mu+1));
                    else
                        bq(j) = (1/(2*(1 - u(j))))^(1/(mu+1));
                    end
                    % Generate the jth element of first child
                    child_1(j) = ...
                        0.5*(((1 + bq(j))*parent_1(j)) + (1 - bq(j))*parent_2(j));
                    % Generate the jth element of second child
                    child_2(j) = ...
                        0.5*(((1 - bq(j))*parent_1(j)) + (1 + bq(j))*parent_2(j));
                    % Make sure that the generated element is within the specified
                    % decision space else set it to the appropriate extrema.
                    if child_1(j) > max_range(j)
                        child_1(j) = max_range(j);
                    elseif child_1(j) < min_range(j)
                        child_1(j) = min_range(j);
                    end
                    if child_2(j) > max_range(j)
                        child_2(j) = max_range(j);
                    elseif child_2(j) < min_range(j)
                        child_2(j) = min_range(j);
                    end
                end
                
            else
                % Initialize the children to be null vector.
                child_1 = [];
                child_2 = [];
                % Select the first parent
                parent_1 = round(N*rand(1));
                if parent_1 < 1
                    parent_1 = 1;
                end
                % Select the second parent
                parent_2 = round(N*rand(1));
                if parent_2 < 1
                    parent_2 = 1;
                end
                % Make sure both the parents are not the same.
                while isequal(parent_chromosome(parent_1,:),parent_chromosome(parent_2,:))
                    parent_2 = round(N*rand(1));
                    if parent_2 < 1
                        parent_2 = 1;
                    end
                end
                % Get the chromosome information for each randomnly selected
                % parents
                parent_1 = parent_chromosome(parent_1,:);
                parent_2 = parent_chromosome(parent_2,:);
                for j = 1 : V
                    child_1(j)=parent_1(j);
                    
                    child_2(j)=parent_2(j);
                end
                
            end
            
            if rand(1) < 0.5
                %do mutation
                delta=[2 2 10 1 1.4];
                for j = 1 : V
                    
                    child_1(j) = child_1(j) + Fn_MiLTester_My_Normal_Rnd(0,delta(j));
                    % Make sure that the generated element is within the decision
                    % space.
                    if child_1(j) > max_range(j)
                        child_1(j) = max_range(j);
                    elseif child_1(j) < min_range(j)
                        child_1(j) = min_range(j);
                    end
                    
                    child_2(j) = child_2(j) + Fn_MiLTester_My_Normal_Rnd(0,delta(j));
                    % Make sure that the generated element is within the decision
                    % space.
                    if child_2(j) > max_range(j)
                        child_2(j) = max_range(j);
                    elseif child_2(j) < min_range(j)
                        child_2(j) = min_range(j);
                    end
                end
            else
                %copy
                for j = 1 : V
                    child_1(j)=child_1(j);
                    
                    child_2(j)=child_2(j);
                end
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% 7. Prescan: Run a simulation for child 1. %%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
           
            
            MinDistPedestrianCar=100;
            TTCMIN=4;
            MinDistPedestrianAwa=50;
            x0P=child_1(1);
            y0P=child_1(2);
            ThP=child_1(3);
            v0P=child_1(4);
            v0C=child_1(5);
            
            MaxD=0;
            Det=0;
            %***
            %%%% Change position and orientation of Pedestrian
            %%%%%%%%% Generate the experiment %%%%%%%%%
            disp('Variables reinitialized...');
            disp('------------------------');
            startTime = rem(now,1);
            startTimeA = rem(now,1);
            Experiment_Name='Experiment_2'; % Hereby enter your experiment name
            PreScan_ExperimentName= [Experiment_Name '.pex']; % add the extension for the PreScan file
            Simulink_ModelName= [ Experiment_Name '_cs'];% add the extension for the Simulink file
            PreScan_ExperimentLocation = [pwd '\' PreScan_ExperimentName];
            simulationCount=0;
            k=0;
            A = cell(3,1);
            B = cell(3,1);
            NbrOfTagAndVal=0;
            
            
            NbrOfTagAndVal=NbrOfTagAndVal+1;
            A{NbrOfTagAndVal}='x_Person';
            radiusvalue={num2str(x0P)};
            idx = randi(numel(radiusvalue),1) ;
            currentName = radiusvalue{idx};
            B{NbrOfTagAndVal}=currentName;
            
            
            NbrOfTagAndVal=NbrOfTagAndVal+1;
            A{NbrOfTagAndVal}='y_Person';
            radiusvalue={num2str(y0P)};
            idx = randi(numel(radiusvalue),1) ;
            currentName = radiusvalue{idx};
            B{NbrOfTagAndVal}=currentName;
            
            NbrOfTagAndVal=NbrOfTagAndVal+1;
            A{NbrOfTagAndVal}='Th_Person';
            radiusvalue={num2str(ThP)};
            idx = randi(numel(radiusvalue),1) ;
            currentName = radiusvalue{idx};
            B{NbrOfTagAndVal}=currentName;
            
            
            for m=1:NbrOfTagAndVal
                clear Tag Val
                Tag(m,:) =A{m};
                Val(m,:)=B{m};
                dos( ['PreScan.CLI.exe -convertAndLoad ' PreScan_ExperimentLocation ' -set ' Tag(m,:) '=' Val(m,:)]);     % Change the value in PreScan
                dos( 'PreScan.CLI.exe -save');         % Save the PreScan experiment
                
            end
            
            dos ('PreScan.CLI.exe --realignPaths');
            dos( 'PreScan.CLI.exe -build');
            
            open_system(Simulink_ModelName);                 % Open the Simulink Model
            
            regenButtonHandle = find_system(Simulink_ModelName, 'FindAll', 'on', 'type', 'annotation','text','Regenerate');
            regenButtonCallbackText = get_param(regenButtonHandle,'ClickFcn');
            
            eval(regenButtonCallbackText);
            
            %%%%
            %Run Simulation
            
            
            sim(fullfile(mfilepath,'/Experiment_2_cs.mdl'));
            maxSimulationsPerViewer=5;
            closeViewer = mod(simulationCount + 1, maxSimulationsPerViewer) == 0; % Close viewer is true if Simulation is > than maxSimulationsPerViewer
            simulationCount = simulationCount + 1;
            if (closeViewer)
                visViewer = 5;
                maxWaitTimeMS = 10*1000;
                nl.tno.wt.prescan.utils.ProcessManagement.Stop(visViewer, maxWaitTimeMS);
                pause(0.5);
                nl.tno.wt.prescan.utils.ProcessManagement.Start(5, maxWaitTimeMS);
            end            %***
            NCSIM=NCSIM+1;
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% 8. NSGAII: Evaluate the objective functions for child 1. %%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            for w=1:length(Detection.signals.values)
                if Detection.signals.values(w)>MaxD
                    MaxD=Detection.signals.values(w);
                end
            end
            if MaxD~=0
                Det=1;
            end
            child_1(6)=Det;
            
            AA=SimStopTime.time;
            b=numel(AA);
            TotSim=AA(b);
            [MinDistPedestrianCar,TTCMIN,MinDistPedestrianAwa]=ObjectiveFunctionsDistancesNew(TotSim,SimulationTimeStep, xCar,yCar,vCar,xPerson,yPerson,vPerson,ThP,TTCcol);
            NFIT=NFIT+1;
            
            
            
            child_1(:,V+2)= MinDistPedestrianCar;            
            child_1(:,V+3) = TTCMIN;
            child_1(:,V+4) = MinDistPedestrianAwa;
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% 8. Prescan: Run a simulation for child 2. %%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%          
           
            MinDistPedestrianCar=100;
            TTCMIN=4;
            MinDistPedestrianAwa=50;
            x0P=child_2(1);
            y0P=child_2(2);
            ThP=child_2(3);
            v0P=child_2(4);
            v0C=child_2(5);
            
            
            MaxD=0;
            Det=0;
            %***
            %%%% Change position and orientation of Pedestrian
            %%%%%%%%% Generate the experiment %%%%%%%%%
            disp('Variables reinitialized...');
            disp('------------------------');
            startTime = rem(now,1);
            startTimeA = rem(now,1);
            Experiment_Name='Experiment_2'; % Hereby enter your experiment name
            PreScan_ExperimentName= [Experiment_Name '.pex']; % add the extension for the PreScan file
            Simulink_ModelName= [ Experiment_Name '_cs'];% add the extension for the Simulink file
            PreScan_ExperimentLocation = [pwd '\' PreScan_ExperimentName];
            simulationCount=0;
            k=0;
            A = cell(3,1);
            B = cell(3,1);
            NbrOfTagAndVal=0;
            
            
            NbrOfTagAndVal=NbrOfTagAndVal+1;
            A{NbrOfTagAndVal}='x_Person';
            radiusvalue={num2str(x0P)};
            idx = randi(numel(radiusvalue),1) ;
            currentName = radiusvalue{idx};
            B{NbrOfTagAndVal}=currentName;
            
            
            NbrOfTagAndVal=NbrOfTagAndVal+1;
            A{NbrOfTagAndVal}='y_Person';
            radiusvalue={num2str(y0P)};
            idx = randi(numel(radiusvalue),1) ;
            currentName = radiusvalue{idx};
            B{NbrOfTagAndVal}=currentName;
            
            NbrOfTagAndVal=NbrOfTagAndVal+1;
            A{NbrOfTagAndVal}='Th_Person';
            radiusvalue={num2str(ThP)};
            idx = randi(numel(radiusvalue),1) ;
            currentName = radiusvalue{idx};
            B{NbrOfTagAndVal}=currentName;
            
            
            for m=1:NbrOfTagAndVal
                clear Tag Val
                Tag(m,:) =A{m};
                Val(m,:)=B{m};
                dos( ['PreScan.CLI.exe -convertAndLoad ' PreScan_ExperimentLocation ' -set ' Tag(m,:) '=' Val(m,:)]);     % Change the value in PreScan
                dos( 'PreScan.CLI.exe -save');         % Save the PreScan experiment
                
            end
            
            dos ('PreScan.CLI.exe --realignPaths');
            dos( 'PreScan.CLI.exe -build');
            
            open_system(Simulink_ModelName);                 % Open the Simulink Model
            
            regenButtonHandle = find_system(Simulink_ModelName, 'FindAll', 'on', 'type', 'annotation','text','Regenerate');
            regenButtonCallbackText = get_param(regenButtonHandle,'ClickFcn');
            
            eval(regenButtonCallbackText);
            
            %%%%
            %Run Simulation
            
            
            sim(fullfile(mfilepath,'/Experiment_2_cs.mdl'));
            maxSimulationsPerViewer=5;
            closeViewer = mod(simulationCount + 1, maxSimulationsPerViewer) == 0; % Close viewer is true if Simulation is > than maxSimulationsPerViewer
            simulationCount = simulationCount + 1;
            if (closeViewer)
                visViewer = 5;
                maxWaitTimeMS = 10*1000;
                nl.tno.wt.prescan.utils.ProcessManagement.Stop(visViewer, maxWaitTimeMS);
                pause(0.5);
                nl.tno.wt.prescan.utils.ProcessManagement.Start(5, maxWaitTimeMS);
            end            %***
            NCSIM=NCSIM+1;
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% 9. NSGAII: Evaluate the objective functions for child 2. %%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            for w=1:length(Detection.signals.values)
                if Detection.signals.values(w)>MaxD
                    MaxD=Detection.signals.values(w);
                end
            end
            if MaxD~=0
                Det=1;
            end
            child_2(6)=Det;
            % TotSim=max(SimStopTime.time);
            AA=SimStopTime.time;
            b=numel(AA);
            TotSim=AA(b);
            [MinDistPedestrianCar,TTCMIN,MinDistPedestrianAwa]=ObjectiveFunctionsDistancesNew(TotSim,SimulationTimeStep, xCar,yCar,vCar,xPerson,yPerson,vPerson,ThP,TTCcol);
            NFIT=NFIT+1;
 
            child_2(:,V+2)= MinDistPedestrianCar;
            child_2(:,V+3) = TTCMIN;
            child_2(:,V+4) = MinDistPedestrianAwa;
            
            
            
            % Keep proper count and appropriately fill the child variable with all
            % the generated children for the particular generation.
            
            child(p,:) = child_1;
            child(p+1,:) = child_2;
            p = p + 2;
            
        end
        offspring_chromosome = child;
        
        %
        
        [main_pop,temp] = size(chromosome);
        [offspring_pop,temp] = size(offspring_chromosome);
        % temp is a dummy variable.
        clear temp
        % intermediate_chromosome is a concatenation of current population and
        % the offspring population.
        intermediate_chromosome(1:main_pop,:) = chromosome;
        intermediate_chromosome(main_pop + 1 : main_pop + offspring_pop,1 : M+V+1) = ...
            offspring_chromosome;
        
        
        intermediate_chromosome = ...
            non_domination_sort_mod(intermediate_chromosome, M, V+1);
        % Perform Selection
        fprintf(fid, 'Intermediate_Chromosome after sorting \n');
        clear InB
        clear InC
        
        InB(:,1:M+V+3)= intermediate_chromosome;
        clear a;
        for i=1:size(InB,1)
            a(:,i)=InB(i,:);
        end
        fprintf(fid, '%.6f  %.6f  %.6f  %.6f  %.6f  %d  %.6f  %.6f  %.6f  %d %.6f \n', a);
        
        
        SimTimeUntilNow=toc
        fprintf(fid, 'SimTimeUntilNow %.3f \n', SimTimeUntilNow);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% 10. NSGAII: Select the best individuals based on elitism and crowding distance. %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        chromosome = replace_chromosome(intermediate_chromosome, M, V+1, pop);
        
        fprintf(fid, 'selected chromosome \n');
        InC(:,1:M+V+3)= chromosome;
        clear a;
        for i=1:size(InC,1)
            a(:,i)=InC(i,:);
        end
        fprintf(fid, '%.6f  %.6f  %.6f  %.6f  %.6f  %d  %.6f  %.6f  %.6f  %d %.6f \n', a);
        
        if ~mod(i,100)
            clc
            fprintf('%d generations completed\n',i);
        end
        
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Print NSGAII results %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf(fid, '\n solution \n');
    clear a;
    for i=1:size(chromosome,1)
        a(:,i)=chromosome(i,:);
    end
    fprintf(fid, '%.6f  %.6f  %.6f  %.6f  %.6f  %d  %.6f  %.6f  %.6f  %d %.6f \n', a);
    
    
    totSimTime=toc
    fprintf(fid, 'totSimTime %.3f \n', totSimTime);
    fprintf(fid, '\n Total number of times we call the sim in about 150mn = %d \n', NCSIM);
    

    
    finishTime=rem(now,1);
    display(strcat('modelRunningTime=',num2str(round((finishTime-startTime)*(24*60))),'mn'));
    fclose(fid);
    
    
    
catch exc
    display(getReport(exc));
    display('Error in model test run!');
end