% % %
%  Copyright (c) {2015-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
%  Raja Ben Abdessalem (benabdessalem@svv.lu)
%  Shiva Nejati (nejati@svv.lu)
%  Briand Lionel (briand@svv.lu)
%  Thomas Stifter (Thomas.Stifter@iee.lu)
%  All rights reserved.
%
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions are
%  met:
%
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
%
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%  POSSIBILITY OF SUCH DAMAGE.

function [of2P, of2Pmin, of2Pmax]=surrogateModelOF2AverageBestR2(child)
%child=[40;40;100;4;20];
data = importdata('1000SolutionsCreated+FitnessValues.txt');
clear x
X=data(:,1:5);
x(1,:)=X(:,1);
Da(1,:)=X(:,1);
x(2,:)=X(:,2);
Da(2,:)=X(:,2);
x(3,:)=X(:,3);
Da(3,:)=X(:,3);
x(4,:)=X(:,4);
Da(4,:)=X(:,4);
x(5,:)=X(:,5);
Da(5,:)=X(:,5);

y1=data(:,6);
t1(1,:)=y1(:,1);
Da(6,:)=y1(:,1);

y2=data(:,7);
t2(1,:)=y2(:,1);
Da(7,:)=y2(:,1);

y3=data(:,8);
t3(1,:)=y3(:,1);
Da(8,:)=y3(:,1);



% vvv=0;
% for i=1:1000
%     if Da(7,i)~=4
%         vvv=vvv+1;
%         Data4(:,vvv)=Da(:,i);
%     end
% end


inputs=x;
% t2Data4(1,:)=Data4(7,:);



    netTTC1=load('netOF2_1');
    netTTC2=load('netOF2_2');
    netTTC3=load('netOF2_3');
    netTTC4=load('netOF2_4');
    netTTC5=load('netOF2_5');
    %of2P= netTTC.net(child);
    
    outputs1 = netTTC1.net(inputs);
    outputs2 = netTTC2.net(inputs);
    outputs3 = netTTC3.net(inputs);
    outputs4 = netTTC4.net(inputs);
    outputs5 = netTTC5.net(inputs);
    
    outputs =  (outputs1 + outputs2 + outputs3 + outputs4 + outputs5)/5;
    
    of2P1= netTTC1.net(child);
    of2P2= netTTC2.net(child);
    of2P3= netTTC3.net(child);
    of2P4= netTTC4.net(child);
    of2P5= netTTC5.net(child);
    
    of2P= (of2P1 + of2P2 + of2P3 + of2P4 + of2P5)/5;
     %error
    
for i=1:1000
AA1(i,:)=t2(:,i);
end

for i=1:1000
D1(i,:)=outputs(:,i);
end
x=AA1;
y=D1;
fitresult = fit(AA1,D1,'poly1');
a=coeffvalues(fitresult);
p1=a(1);
p2=a(2);
%output=p1*target+p2
TargetOF2=(of2P - p2) / p1;

% 
% ci = confint(fitresult,0.95)
% a_ci = ci(:,1);
% b_ci = ci(:,2);
% 
% 
% of1Pmin=a_ci(1)*TargetOF1+b_ci(1);
% of1Pmax=a_ci(2)*TargetOF1+b_ci(2);

[ci]=predint(fitresult,TargetOF2,0.95,'observation','on');
of2Pmin=ci(1);
if of2Pmin < 0
    of2Pmin=0;
end

if of2Pmin>4
    of2Pmin=4;
    
end
of2Pmax=ci(2);

if of2Pmax > 4
    of2Pmax=4;
end

if of2Pmax<0
    of2Pmax=0;
end

if of2P <0
    of2P=0;
end
if of2P>4
    of2P=4;
end

