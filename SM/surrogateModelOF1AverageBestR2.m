% % %
%  Copyright (c) {2015-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
%  Raja Ben Abdessalem (benabdessalem@svv.lu)
%  Shiva Nejati (nejati@svv.lu)
%  Briand Lionel (briand@svv.lu)
%  Thomas Stifter (Thomas.Stifter@iee.lu)
%  All rights reserved.
%
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions are
%  met:
%
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
%
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%  POSSIBILITY OF SUCH DAMAGE.

function [of1P, of1Pmin, of1Pmax]=surrogateModelOF1AverageBestR2(child)
%child=[40;40;100;4;20];
data = importdata('1000SolutionsCreated+FitnessValues.txt');
clear x
X=data(:,1:5);
x(1,:)=X(:,1);
Da(1,:)=X(:,1);
x(2,:)=X(:,2);
Da(2,:)=X(:,2);
x(3,:)=X(:,3);
Da(3,:)=X(:,3);
x(4,:)=X(:,4);
Da(4,:)=X(:,4);
x(5,:)=X(:,5);
Da(5,:)=X(:,5);

y1=data(:,6);
t1(1,:)=y1(:,1);
Da(6,:)=y1(:,1);

y2=data(:,7);
t2(1,:)=y2(:,1);
Da(7,:)=y2(:,1);

y3=data(:,8);
t3(1,:)=y3(:,1);
Da(8,:)=y3(:,1);

inputs=x;

%netDtoCar=load('netOF1');
%of1P= netDtoCar.net(child);
%outputs = netDtoCar.net(inputs);



    netDtoCar1=load('netOF1_1');
    netDtoCar2=load('netOF1_2');
    netDtoCar3=load('netOF1_3');
    netDtoCar4=load('netOF1_4');
    netDtoCar5=load('netOF1_5');
    %of2P= netTTC.net(child);
    
    outputs1 = netDtoCar1.net(inputs);
    outputs2 = netDtoCar2.net(inputs);
    outputs3 = netDtoCar3.net(inputs);
    outputs4 = netDtoCar4.net(inputs);
    outputs5 = netDtoCar5.net(inputs);
    
    outputs =  (outputs1 + outputs2 + outputs3 + outputs4 + outputs5)/5;
    
    of1P1= netDtoCar1.net(child);
    of1P2= netDtoCar2.net(child);
    of1P3= netDtoCar3.net(child);
    of1P4= netDtoCar4.net(child);
    of1P5= netDtoCar5.net(child);
    
    of1P= (of1P1 + of1P2 + of1P3 + of1P4 + of1P5)/5;
    
for i=1:1000
AA1(i,:)=t1(:,i);
end

for i=1:1000
D1(i,:)=outputs(:,i);
end
x=AA1;
y=D1;
fitresult = fit(AA1,D1,'poly1');
a=coeffvalues(fitresult);
p1=a(1);
p2=a(2);
%output=p1*target+p2
TargetOF1=(of1P - p2) / p1;

% 
% ci = confint(fitresult,0.95)
% a_ci = ci(:,1);
% b_ci = ci(:,2);
% 
% 
% of1Pmin=a_ci(1)*TargetOF1+b_ci(1);
% of1Pmax=a_ci(2)*TargetOF1+b_ci(2);

[ci]=predint(fitresult,TargetOF1,0.95,'observation','on');
of1Pmin=ci(1);
if of1Pmin < 0
    of1Pmin=0;
end

if of1Pmin>85
    of1Pmin=85;
end

of1Pmax=ci(2);

if of1Pmax > 85
    of1Pmax=85;
end

if of1Pmax <0
    of1Pmax=0;
end
if of1P <0
    of1P=0;
end
if of1P>85
    of1P=85;
end
