% % %
%  Copyright (c) {2015-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
%  Raja Ben Abdessalem (benabdessalem@svv.lu)
%  Shiva Nejati (nejati@svv.lu)
%  Briand Lionel (briand@svv.lu)
%  Thomas Stifter (Thomas.Stifter@iee.lu)
%  All rights reserved.
%
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions are
%  met:
%
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
%
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%  POSSIBILITY OF SUCH DAMAGE.

function [f] = deleteOneElement (intermediate_chromosomeP, IW, M, V);
f=[];
k=0;
[N1, m1] = size(intermediate_chromosomeP);
[N2, m2] = size(IW);
bb= false;

if N2 == 0
    f=intermediate_chromosomeP;
else
for i=1: N1
    bb= false;
    %for j=1:N2   
        if intermediate_chromosomeP(i,1:V)==IW(1,1:V)
            bb=true;
          
        end
    %end
    
    if bb == false
        k=k+1;
        f(k,:)=intermediate_chromosomeP(i,:);
    end
end
end
