% % %
%  Copyright (c) {2015-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
%  Raja Ben Abdessalem (benabdessalem@svv.lu)
%  Shiva Nejati (nejati@svv.lu)
%  Briand Lionel (briand@svv.lu)
%  Thomas Stifter (Thomas.Stifter@iee.lu)
%  All rights reserved.
%
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions are
%  met:
%
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
%
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%  POSSIBILITY OF SUCH DAMAGE.


function []=savenetOf3LMDistanceNew()
%child=[40;40;100;4;20];
data = importdata('New1000SolutionsCreated+FitnessValues.txt');
indices = importdata('7indicesOF3LMDistance.txt');
clear inputTestSet
clear targetTestSet
clear outputTestSet
clear inputTestsSeti
clear outputTestsSeti
clear targetTestSeti
clear y1
clear y2
clear y3
clear t1
clear t2
clear t3
clear x;
clear train
clear test
clear trInd
clear tstInd
clear trainset
clear testset
clear net

X=data(:,1:5);
x(1,:)=X(:,1);
Da(1,:)=X(:,1);
x(2,:)=X(:,2);
Da(2,:)=X(:,2);
x(3,:)=X(:,3);
Da(3,:)=X(:,3);
x(4,:)=X(:,4);
Da(4,:)=X(:,4);
x(5,:)=X(:,5);
Da(5,:)=X(:,5);

y1=data(:,6);
t1(1,:)=y1(:,1);
Da(6,:)=y1(:,1);

y2=data(:,7);
t2(1,:)=y2(:,1);
Da(7,:)=y2(:,1);

y3=data(:,8);
t3(1,:)=y3(:,1);
Da(8,:)=y3(:,1);




inputs=x;

% Create a Pattern Recognition Network
    hiddenLayerSize = 3;
   % net = patternnet(hiddenLayerSize);
    net = fitnet(hiddenLayerSize);

tt=1;
bb=1;
for i=1:5
k=0;
l=0;
outputTestSeti=zeros(1,200);
ss=1;
for j=1:1000
    
if (indices(j)==i)
    k=k+1;
tstInd(k)=j;
testset(:,k)=Da(:,j);
targetTestSet(1,tt)=Da(8,j);
inputTestsSeti(1:5,ss)=Da(1:5,j);
tt=tt+1;
ss=ss+1;
else
    l=l+1;
trInd(l)=j;
trainset(:,l)=Da(:,j);
end

end
%save targetTestSet.txt targetTestSet -ASCII
net.trainFcn = 'trainlm';
net.trainParam.epochs = 100;
net.divideFcn = 'divideind'; 
net.divideParam.trainInd=trInd;
net.divideParam.testInd=tstInd;
net.performFcn = 'mse';
%Of1

% Train the Network
    [net,tr] = train(net,inputs,t3);
 if i==1
        save ('netOF3_1', 'net');
    end
    if i==2
        save ('netOF3_2', 'net');
    end
    if i==3
        save ('netOF3_3', 'net');
    end
    if i==4
        save ('netOF3_4', 'net');
    end
    
    if i==5
        save ('netOF3_5', 'net');
    end
end