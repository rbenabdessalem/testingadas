% % %
%  Copyright (c) {2015-2019}, University of Luxembourg, Interdisciplinary Centre for Security, Reliability and Trust (SnT)
%  Raja Ben Abdessalem (benabdessalem@svv.lu)
%  Shiva Nejati (nejati@svv.lu)
%  Briand Lionel (briand@svv.lu)
%  Thomas Stifter (Thomas.Stifter@iee.lu)
%  All rights reserved.
%
%  Redistribution and use in source and binary forms, with or without
%  modification, are permitted provided that the following conditions are
%  met:
%
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
%
%  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%  POSSIBILITY OF SUCH DAMAGE.

function [P,maxcd]=highestCDwithLowRankinB(intermediate_chromosomeBR1Predict,intermediate_chromosomeB, M, V)
clear P;
indB=0;
indW=0;
P=intermediate_chromosomeBR1Predict(1,:);
maxcd=intermediate_chromosomeBR1Predict(1,M+V+2);
N1=size(intermediate_chromosomeBR1Predict,1);
N2=size(intermediate_chromosomeB,1);
f=[];
l=[];
x=[];
k=0;
y=[];
for i=1:N1
    if intermediate_chromosomeBR1Predict(i,M+V+2)> maxcd
        maxcd = intermediate_chromosomeBR1Predict(i,M+V+2);
        %P=intermediate_chromosomeBR1Predict(i,:);
    end
    
    
end

for i=1:N1
    if intermediate_chromosomeBR1Predict(i,M+V+2)== maxcd
        %maxcd = intermediate_chromosomeBR1Predict(i,M+V+2);
        k=k+1;
        f(k,:)=intermediate_chromosomeBR1Predict(i,:);
        x(k)= i;
    end
    
end
if size(f,1) ==1 
    P=f(1,:);
    indW=x(1);
else
minRankBinF=10;
for i=1:N2
    for j=1: size(f,1)
    if intermediate_chromosomeB(i,1:M)== f(j, 1:M)
        if intermediate_chromosomeB(i,M+V+1)< minRankBinF
            minRankBinF=intermediate_chromosomeB(i,M+V+1);
            l(1,:)= f(j,:);
            indB=i;
            indW=j;
        end
    end
    end
    
end

P=l;
end